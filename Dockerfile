FROM nvidia/cuda:11.3.1-cudnn8-devel-ubuntu20.04

MAINTAINER ekatsym

ENV DEBIAN_FRONTEND=noninteractive
ENV LC_ALL=C.UTF-8 LANG=C.UTF-8

RUN apt update \
 && apt install -y curl git-core python3.8 python3-pip python-is-python3 python3-venv python3-tk libopencv-dev \
 && apt clean \
 && rm -rf /var/lib/apt/lists/*

RUN pip install --upgrade pip
RUN pip install pipenv

RUN git clone https://github.com/Stability-AI/stablediffusion.git /workspace \
 && cd /workspace \
 && /usr/local/bin/pipenv --python 3.8 \
 && /usr/local/bin/pipenv install torch numpy tqdm \
 && /usr/local/bin/pipenv install \
    albumentations==0.4.3 \
    opencv-python \
    imageio==2.9.0 \
    imageio-ffmpeg==0.4.2 \
    pytorch-lightning==1.4.2 \
    torchmetrics==0.6 \
    omegaconf==2.1.1 \
    'test-tube>=0.7.5' \
    'streamlit>=0.73.1' \
    einops==0.3.0 \
    transformers==4.19.2 \
    webdataset==0.2.5 \
    open-clip-torch==2.7.0 \
    gradio==3.13.2 \
    kornia==0.6 \
    'invisible-watermark>=0.1.5' \
    streamlit-drawable-canvas==0.8.0 \
 && /usr/local/bin/pipenv install --dev ipython notebook

WORKDIR /workspace


CMD ["/bin/bash"]
